<?php
/**
 * Footer HTML markup structure.
 *
 * @package     PEDRO\PedroTraining
 * @since       1.0.0
 * @author      Purple Prodigy
 * @link        http://www.purpleprodigy.com
 * @licence     GNU General Public License 2.0+
 */

namespace PEDRO\PedroTraining;

add_filter( 'genesis_footer_creds_text', __NAMESPACE__ . '\show_language_specific_footer' );
/**
 * Customise footer credits depending on language.
 *
 * @since 1.0.0
 *
 * @return void
 */
function show_language_specific_footer() {
	if ( current_user_can( 'mepr-active', 'membership:801' ) ) {
		include CHILD_THEME_DIR . '/src/views/footer-portuguese.php';

	} else {
		include CHILD_THEME_DIR . '/src/views/footer-english.php';
	}
}
