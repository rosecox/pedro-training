<?php
/**
 * Use conditional menus for subscribers.
 *
 * @package     PEDRO\PedroTraining
 * @since       1.1.0
 * @author      Purple Prodigy
 * @link        http://www.purpleprodigy.com
 * @licence     GNU General Public License 2.0+
 */

namespace PEDRO\PedroTraining;

add_action( 'genesis_header_right', __NAMESPACE__ . '\show_conditional_account_menu' );

/**
 * Show English account menu on parent and sub-pages of English template only for logged in users and
 * show Portuguese account menu on parent and sub-pages of Portuguese template only for logged in users
 * otherwise show login link.
 *
 * @since 1.1.0
 *
 * @return void
 */
function show_conditional_account_menu() {

	if ( genesis_a11y( 'headings' ) ) {
		printf( '<h2 class="screen-reader-text">%s</h2>', __( 'Account navigation', CHILD_TEXT_DOMAIN ) );
	}

	$theme_location = get_user_menu_preference();
	if (!$theme_location) {
		$theme_location = 'login';
		//include CHILD_THEME_DIR . '/src/views/login.php';
	} else {
		$theme_location = 'account-' . $theme_location;
	}

	genesis_nav_menu( array(
		'theme_location' => $theme_location,
		'menu_class'     => $class,
	) );
}

add_action( 'genesis_after_header', __NAMESPACE__ . '\show_conditional_nav_menu' );
/**
 * Show English menu to parent and sub-pages of English template only and
 * show Portuguese menu to parent and sub-pages of Portuguese template only
 * otherwise show no menu.
 *
 * @since 1.1.0
 *
 * @return void
 */
function show_conditional_nav_menu() {
	$class = 'menu genesis-nav-menu menu-membership';
	if ( genesis_superfish_enabled() ) {
		$class .= ' js-superfish';
	}

	if ( genesis_a11y( 'headings' ) ) {
		printf( '<h2 class="screen-reader-text">%s</h2>', __( 'Main navigation', CHILD_TEXT_DOMAIN ) );
	}

	$theme_location = get_user_menu_preference();
	if (!$theme_location) {
		return;
	}

	genesis_nav_menu( array(
		'theme_location' => $theme_location,
		'menu_class'     => $class,
	) );
}

/**
 * Get English or Portuguese membership preference.
 *
 * @since 1.1.0
 *
 * @return string
 */
function get_user_menu_preference() {
	if ( current_user_can( 'mepr-active', 'membership:111' ) ) {
		return 'english';
	}

	if ( current_user_can( 'mepr-active', 'membership:801' ) ) {
		return 'portuguese';
	}

	return '';
}
