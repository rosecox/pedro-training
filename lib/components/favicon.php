<?php
/**
 * Add custom favicon.
 *
 * @package     PEDRO\PedroTraining
 * @since       1.0.0
 * @author      Purple Prodigy
 * @link        http://www.purpleprodigy.com
 * @licence     GNU General Public License 2.0+
 */
namespace PEDRO\PedroTraining;

/**
 * Add custom favicon for PEDro
 *
 * @since 1.0.0
 *
 * @return void
 */
function pedro_favicon() {
	include CHILD_THEME_DIR . '/src/views/favicon.php';
}
add_action( 'wp_head', __NAMESPACE__ . '\pedro_favicon' );
remove_action( 'wp_head', 'genesis_load_favicon' );