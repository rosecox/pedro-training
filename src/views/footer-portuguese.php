<p>Copyright &copy; <?php the_date( 'Y' ); ?> <a href="https://training.pedro.org.au">Physiotherapy Evidence Database (PEDro)</a> &middot; <a href="/sitemap/" rel="nofollow">Sitemap</a> &middot; <a href="/terms/" rel="nofollow">Terms &amp; Conditions</a> &middot; Website by <a href="https://purpleprodigy.com">Purple Prodigy</a></p>
<div class="social-icons">
	<ul>
		<li><a href="https://www.facebook.com/PhysiotherapyEvidenceDatabase.PEDrinho"><i class="fa fa-facebook"><span class="screen-reader-text">Like us on Facebook</span></i></a></li>
		<li><a href="https://twitter.com/PEDrinho_CEBP"><i class="fa fa-twitter"><span class="screen-reader-text">Follow us on Twitter</span></i></a></li>
		<li><a href="https://plus.google.com/107407945079622420907"><i class="fa fa-google-plus"><span class="screen-reader-text">Connect on Google+</span></i></a></li>
		<li><a href="https://www.youtube.com/playlist?list=PLoyl60sa_9j-kudVtowbCjsxa2ZrV5CLp"><i class="fa fa-youtube"><span class="screen-reader-text">Follow us on YouTube</span></i></a></li>
	</ul>
</div>
