<li class="menu-item menu--signout">
	<a href="<?php echo esc_url( wp_logout_url( $signinout_redirect ) ); ?>" itemprop="url"><span itemprop="name" class="menu-item--text">Log Out</span></a>
</li>
